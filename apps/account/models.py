from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _

from model_utils.fields import MonitorField
from model_utils.models import SoftDeletableModel
from phonenumber_field.modelfields import PhoneNumberField

from tokens.models import MultiToken


class User(AbstractUser, SoftDeletableModel):

    email = models.EmailField(_('email address'), unique=True)
    mobile = PhoneNumberField(null=True, unique=True)
    mobile_verified = models.BooleanField(default=False)
    email_verified = models.BooleanField(default=False)

    mobile_verified_on = MonitorField(monitor='mobile_verified', when=[True], blank=True, null=True)
    email_verified_on = MonitorField(monitor='email_verified', when=[True], blank=True, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return "{}, {}".format(self.email, self.get_full_name())

    def get_auth_token(self, key):
        return "Token "+ MultiToken.objects.create(user_id=self.id).key

    def delete_auth_token(self, key):
        MultiToken.objects.filter(key=key).delete()

    def logout_all(self):
        MultiToken.objects.filter(user_id=self.id).delete()
