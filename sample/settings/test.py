from .base import *

REST_FRAMEWORK.update(
    {
        'TEST_REQUEST_DEFAULT_FORMAT': 'json'
    }
)